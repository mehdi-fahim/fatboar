pipeline {
  agent any
    
  stages {
    stage("Build") {
      steps {
        mail bcc: '', body: 'Le build commence ', cc: 'jenkins', from: '', replyTo: '', subject: "[Fatboar] build ${BUILD_NUMBER}", to: 'sabertaby@gmail.com'        
      }
    }

    stage("Tests") {
      environment {
        DB_TEST     = "db_test_${BRANCH_NAME}_${BUILD_ID}"
        APP_TEST    = "app_test_${BRANCH_NAME}_${BUILD_ID}"
        CURRENT_DIR = "/lab/Lab"
      }

      steps {
          dir("$WORKSPACE") {
            // Create container for testing
            sh "docker run -d --name $DB_TEST -e 'POSTGRES_USER=fatboar_test' -e 'POSTGRES_PASSWORD=fatboar_test_password' -e 'POSTGRES_DB=fatboar_test' postgres:11.2"
            sh "docker run -di --name $APP_TEST -v $WORKSPACE:/lab -w /lab -e \"DATABASE_URL=pgsql://fatboar_test:fatboar_test_password@${DB_TEST}/fatboar_test\" --link $DB_TEST registry.furious-ducks.website/fb_app:1.0-dev"
            
            // Copy Lab into app container
            sh "docker cp $WORKSPACE/Lab $APP_TEST:/lab/"
            
            // Install deps
            sh "docker exec -w $CURRENT_DIR -i $APP_TEST composer install"
            sh "docker exec -w $CURRENT_DIR -i $APP_TEST composer dump-autoload --optimize --classmap-authoritative"
            sh "docker exec -w $CURRENT_DIR -i $APP_TEST php bin/console doctrine:database:create --if-not-exists --connection=default"
            sh "docker exec -w $CURRENT_DIR -i $APP_TEST php bin/console doctrine:schema:update --force"
            sh "docker exec -w $CURRENT_DIR -i $APP_TEST php bin/console doctrine:fixtures:load --no-interaction --append"
            // start testings
            sh "docker exec -w $CURRENT_DIR -i $APP_TEST ./bin/phpunit --log-junit tests/tests.xml"
            
            // Cp tests file into workspace from containers
            sh "docker cp $APP_TEST:$CURRENT_DIR/tests/tests.xml $WORKSPACE/tests.xml"
            
            // remove testing's containers  
            sh "docker rm -f $APP_TEST $DB_TEST"

            junit "tests.xml"
            archiveArtifacts artifacts: "tests.xml"
            
          }
      }
    }

    stage("Deploy MASTER") {
      when  {
        branch 'master'
      }
      steps {
       withCredentials([
          sshUserPrivateKey(credentialsId: '876cada9-4fd4-4f95-9a76-bf36c4fb4241', keyFileVariable: 'keyfile', passphraseVariable: 'passphrase', usernameVariable: 'usename'),
          file(credentialsId: '21412669-97e5-4f15-ae69-0659724a9a33', variable: 'vault_file')
          ]) {
            sh "ANSIBLE_CONFIG=Ansible/ansible.cfg ansible-playbook -i Ansible/inventories/hosts.yaml --private-key ${keyfile} --vault-password-file ${vault_file} Ansible/production.yml -vvv"
        }
      }

    }
    
    stage("Deploy Develop") {
      when  {
        branch 'develop'
      }
      steps {
        withCredentials([
          sshUserPrivateKey(credentialsId: '876cada9-4fd4-4f95-9a76-bf36c4fb4241', keyFileVariable: 'keyfile', passphraseVariable: 'passphrase', usernameVariable: 'usename'),
          file(credentialsId: '21412669-97e5-4f15-ae69-0659724a9a33', variable: 'vault_file')
          ]) {
            sh "ANSIBLE_CONFIG=Ansible/ansible.cfg ansible-playbook -i Ansible/inventories/hosts.yaml --private-key ${keyfile} --vault-password-file ${vault_file} Ansible/develop.yml -vvv"
        }
      }
    }
    
    stage("Deploy int") {
      when  {
        branch 'int'
      }
      steps {
        echo "Pas de server d'int dédiée"
      }
    }
  }

  post {
    always {
      cleanWs()
    }
    success { 
      echo 'I will say Hello on success !'
      mail bcc: '', body: 'Le build a réussi ', cc: 'jenkins', from: '', replyTo: '', subject: "[Fatboar] build ${BUILD_NUMBER}", to: 'sabertaby@gmail.com'        
    }
  }
}
