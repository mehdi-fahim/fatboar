# Fatboar Instance

## Prérequis

* Docker

## First Sets

1. activate virtualenv 
2. install requirements: `pip install -r requirements.txt`

## Run Tests

```bash
docker run --rm -it \
-v "$(pwd)":/tmp/$(basename "${PWD}") \
-v /var/run/docker.sock:/var/run/docker.sock \
-w /tmp/$(basename "${PWD}") \
quay.io/ansible/molecule:2.20 \
molecule test
```
