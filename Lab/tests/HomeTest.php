<?php

namespace Tests\App;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class HomeTest extends WebTestCase
{
    public function testGetHomePage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertContains('JEU CONCOURS FATBOAR', $crawler->filter('h1')->text());

    }

    public function testGetTicketNotLogin() 
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('form[search]')->form();
        $client->submit($form);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Vous devez vous connecter pour entrer votre ticket', $crawler->filter('#msgErreurformHome')->text());
    }
}
