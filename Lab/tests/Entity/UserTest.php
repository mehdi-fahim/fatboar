<?php 

namespace App\Tests\Entity;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends KernelTestCase {
    
    // use FixturesTrait;

    public function getEntity() : User {
        return (new User())
            ->setFirstName("George")
            ->setFamilyName("DUPONT")
            ->setEmail("george@hotmail.com")
            ->setImageUrl("https://freesvg.org/img/icon_user_greyonwhite.png")
            ->setCheckCgu(true)
            ->setCheckNewsletter(false);
    }


    public function assertHasErrors(User $user, int $nb_error = 0) {

        self::bootKernel();
        $error = self::$container->get('validator')->validate($user);
        $this->assertCount($nb_error, $error);
    }

    // public function assertHasErrorsType($type, $field) {

    //     self::bootKernel();
    //     $error = self::$container->get('validator')->validate($user);
    //     $this->assertFalse($this->assertInternalType($type, $field));
    // }

    public function testValidEntity() 
    {
        $this->assertHasErrors($this->getEntity(), 0);
        // $this->assertHasErrorsType("string", $this->getEntity()->getEmail());
    }

    public function testInvalidImageEntity() 
    {
        $this->assertHasErrors($this->getEntity()->setImageUrl(""), 1);
        $this->assertHasErrors($this->getEntity()->setImageUrl("not_url"), 1);
    }

    public function testInvalidEmailEntity() 
    {
        $this->assertHasErrors($this->getEntity()->setEmail(""), 1);
        $this->assertHasErrors($this->getEntity()->setEmail("not_email"), 1);
    }

    // public function testUniqueField()
    // {
    //     $this->loadFixtureFiles([dirname(__DIR__).'/FixturesTest/user_test.yaml']);
    //     $this->assertHasErrors($this->getEntity()->setEmail("jesuisuntest@gmail.com"), 1);
    // }

    public function testInvalidFirstNameEntity() 
    {
        $this->assertHasErrors($this->getEntity()->setFirstName(""), 1);
        // $this->assertHasErrors($this->getEntity()->setFirstName(3456), 1);
    }

    public function testInvalidFamilyNameEntity() 
    {
        $this->assertHasErrors($this->getEntity()->setFamilyName(""), 1);
        // $this->assertHasErrors($this->getEntity()->setFamilyName(45678), 1);
    }
}
