<?php

namespace Tests\App;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CguTest extends WebTestCase {

    public function test200CGUPage()
    {
        $client = static::createClient();
        $client->request('GET', '/cgu');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testH1CGUPage()
    {
        $client = static::createClient();
        $client->request('GET', '/cgu');
        $this->assertSelectorTextContains('h1', "Condition Générale d'Utilisation");
    }

}
