<?php

namespace Tests\App;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProfilTest extends WebTestCase {

    public function test301ProfilPage()
    {
        $client = static::createClient();
        $client->request('GET', '/profil');
        $this->assertResponseStatusCodeSame(301);
    }
    
}
