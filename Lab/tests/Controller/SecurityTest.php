<?php

namespace Tests\App;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityTest extends WebTestCase {

    public function test200InscriptionPage()
    {
        $client = static::createClient();
        $client->request('GET', '/inscription');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testH1InscriptionPage()
    {
        $client = static::createClient();
        $client->request('GET', '/inscription');
        $this->assertSelectorTextContains('h1', "Formulaire d'inscription - Fatboar");
    }

}
