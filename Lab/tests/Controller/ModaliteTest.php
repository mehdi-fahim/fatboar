<?php

namespace Tests\App;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ModaliteTest extends WebTestCase {

    public function test200ModalitePage()
    {
        $client = static::createClient();
        $client->request('GET', '/modalite');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testH1ModalitePage()
    {
        $client = static::createClient();
        $client->request('GET', '/modalite');
        $this->assertSelectorTextContains('h1', "Modalité du jeu");
    }

}
