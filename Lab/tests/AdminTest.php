<?php
/**
 * Author: MFAHIM
 * Date: 25/05/2018
 *
 */

namespace Tests\App;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminTest extends WebTestCase
{
    public function testGetUsers()
    {
        $client = static::createClient();

        $client->request('GET', '/admin/user');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    // public function testGetOneUser()
    // {
    //     $client = static::createClient();

    //     $client->request('GET', '/admin/user/show/1');
        
    //     $this->assertSelectorTextContains('h1', "Vous n'êtes pas connecté");
    // }

    public function testGetTickets()
    {
        $client = static::createClient();

        $client->request('GET', '/admin/ticket');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    // public function testGetOneTicket()
    // {
    //     $client = new \GuzzleHttp\Client( [
    //         'base_uri' => 'http://127.0.0.1:8000',
    //         'timeout'  => 10
    //     ] );

    //     $response = $client->get('/admin/ticket/show/1');
    //     $this->assertEquals(200, $response->getStatusCode());
    // }

    public function testGetPrize()
    {
        $client = static::createClient();

        $client->request('GET', '/admin/prize');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    // public function testGetOnePrize()
    // {
    //     $client = new \GuzzleHttp\Client( [
    //         'base_uri' => 'http://127.0.0.1:8000',
    //         'timeout'  => 10
    //     ] );

    //     $response = $client->get('/admin/prize/show/1');
    //     $this->assertEquals(200, $response->getStatusCode());
    // }

    public function testGetSessions()
    {
            $client = static::createClient();

            $client->request('GET', '/admin/session');
            $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    // public function testGetOneSessions()
    // {
    //     $client = new \GuzzleHttp\Client( [
    //         'base_uri' => 'http://127.0.0.1:8000',
    //         'timeout'  => 10
    //     ] );

    //     $response = $client->get('/admin/session/show/1');
    //     $this->assertEquals(200, $response->getStatusCode());
    // }
}
