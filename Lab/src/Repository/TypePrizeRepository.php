<?php

namespace App\Repository;

use App\Entity\TypePrize;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypePrize|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypePrize|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypePrize[]    findAll()
 * @method TypePrize[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypePrizeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypePrize::class);
    }

    // /**
    //  * @return TypePrize[] Returns an array of TypePrize objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypePrize
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
