<?php

namespace App\Repository;

use App\Entity\Ticket;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Ticket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ticket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ticket[]    findAll()
 * @method Ticket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicketRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    // /**
    //  * @return Ticket[] Returns an array of Ticket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ticket
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
    * @return Query
    */
    public function findAllVisibleQuery(): query
    {
        return $this->createQueryBuilder('t')
            ->orderBy("t.id", "asc")
            ->getQuery();
    }

    public function searchTicket($value): array
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.ticket_no LIKE :val')
            ->setParameter('val', '%'.$value.'%')
            ->getQuery()
            ->getResult();
    }

    public function searchTicketByUser($value, $user): array
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.ticket_no LIKE :val')
            ->andWhere('t.user = :id')
            ->setParameter('val', '%'.$value.'%')
            ->setParameter('id', $user)
            ->getQuery()
            ->getResult();
    }

    public function checkTicket($value): array
    {
        return $this->createQueryBuilder('t')
        ->andWhere('t.ticket_no = :val')
        ->setParameter('val', $value)
        ->getQuery()
        ->getResult();
    }
}
