<?php

namespace App\Repository;

use App\Entity\Prize;
use App\Entity\Ticket;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Stats|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stats|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stats[]    findAll()
 * @method Stats[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ticket::class);
        // parent::__construct($registry, Prize::class);
    }

    public function countTicketIsUsed(): array
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->where('t.is_used = true')
            ->getQuery()
            ->getResult();
    }

    public function countTicketIsUsedPrize1(): array
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->where('t.is_used = true')
            ->andWhere('t.prize = 1')
            ->getQuery()
            ->getResult();
    }

    public function countTicketIsUsedPrize2(): array
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->where('t.is_used = true')
            ->andWhere('t.prize = 2')
            ->getQuery()
            ->getResult();
    }

    public function countTicketIsUsedPrize3(): array
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->where('t.is_used = true')
            ->andWhere('t.prize = 3')
            ->getQuery()
            ->getResult();
    }

    public function countTicketIsUsedPrize4(): array
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->where('t.is_used = true')
            ->andWhere('t.prize = 4')
            ->getQuery()
            ->getResult();
    }

    public function countTicketIsUsedPrize5(): array
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->where('t.is_used = true')
            ->andWhere('t.prize = 5')
            ->getQuery()
            ->getResult();
    }


    public function countTicketIsRecover(): array
    {
        return $this->createQueryBuilder('t')
            ->select('count(t.id)')
            ->where('t.is_recover = true')
            ->getQuery()
            ->getResult();
    }

    // public function countGetPrize(): array
    // {
    //     return $this->createQueryBuilder('p')
    //         ->select('count(p.id)')
    //         ->getQuery()
    //         ->getResult();
    // }
}
