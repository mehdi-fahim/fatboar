<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class UserController extends AbstractController
{
    /**
    * @Route("/admin/user/{page<\d+>?1}", name="user_index", methods={"GET", "POST"})
    * @IsGranted("IS_AUTHENTICATED_FULLY", message="Vous n'ètes pas connécté")
    */
    public function index(UserRepository $userRepository, Request $request, $page, PaginatorInterface $paginator): Response
    {
        $limit = 10;

        $searchBar = $this->createFormBuilder()
            ->add('query', TextType::class,array(
                'label' => false,
                'attr' => [
                    'class' => 'input is-primary inputSearchTicket',
                    'placeholder' => 'george@gmail.com',
                ]
            ))
            ->add('search', SubmitType::class, [
                'label' => 'Rechercher',
                'attr' => [
                    'class' => 'button is-primary btnSearchAdmin',
                ]
            ])->getForm();

        $searchBar->handleRequest($request);

        $queryUser = $request->request->get('form')['query'];
        $resSearch = $userRepository->searchUser($queryUser);

        if ($resSearch) {
            $users = $paginator->paginate(
                $resSearch,
                $page,
                $limit
            );
        } else {
            $users = $paginator->paginate(
                $userRepository->findAllVisibleQuery(),
                $page,
                $limit
            );
        }

        return $this->render('user/index.html.twig',[
            'users' => $users,
            'searchBar' => $searchBar->createView()
        ]);
    }


    /**
     * @Route("/admin/user/{id}/delete", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user, $id): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('DELETE App:User id  WHERE id = :userid')->setParameter("userid", $user->getId());
            $result = $query->execute();    
        }
        
        return $this->redirectToRoute('user_index');
    }


    /**
     * @Route("/admin/user/show/{id}", name="user_show", methods={"GET"})
     * @IsGranted("IS_AUTHENTICATED_FULLY", statusCode=401, message="Vous n'êtes pas connecté")
     */
    public function show(UserRepository $userRepository, User $user, $id): Response
    {
        $userRole = $userRepository->getRoleUser($id);

        return $this->render('user/show.html.twig', [
            'user' => $user,
            'role' => $userRole[0]
        ]);
    }
    
}
