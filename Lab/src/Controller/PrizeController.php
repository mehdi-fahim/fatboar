<?php

namespace App\Controller;

use App\Entity\Prize;
use App\Form\PrizeType;
use App\Repository\PrizeRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin/prize")
 */
class PrizeController extends AbstractController
{
    /**
     * @Route("/{page<\d+>?1}", name="prize_index", methods={"GET", "POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY", message="Vous n'ètes pas connécté")
     */
    public function index(Request $request, PrizeRepository $prizeRepository, $page, PaginatorInterface $paginator): Response
    {
        $prize = new Prize();
        $form = $this->createForm(PrizeType::class, $prize);
        $form->handleRequest($request);

        $limit = 10;

        $user = $this->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $prize->setUser($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($prize);
            $entityManager->flush();

            return $this->redirectToRoute('prize_index');
        }

        $searchBar = $this->createFormBuilder()
            ->add('query', TextType::class,array(
                'label' => false,
                'attr' => [
                    'class' => 'input is-primary inputSearchTicket',
                    'placeholder' => 'Burger',
                ]
            ))
            ->add('search', SubmitType::class, [
                'label' => 'Rechercher',
                'attr' => [
                    'class' => 'button is-primary btnSearchAdmin',
                ]
            ])->getForm();

        $searchBar->handleRequest($request);

        $queryPrix = $request->request->get('form')['query'];
        $resSearch = $prizeRepository->searchPrize($queryPrix);

        if ($resSearch) {
            $prix = $paginator->paginate(
                $resSearch,
                $page,
                $limit
            );
        } else {
            $prix = $paginator->paginate(
                $prizeRepository->findAllVisibleQuery(),
                $page,
                $limit
            );
        }  

        return $this->render('prize/index.html.twig', [
            'prizes' => $prix,
            'form' => $form->createView(),
            'searchBar' => $searchBar->createView()
        ]);
    }

    

    /**
     * @Route("/show/{id}", name="prize_show", methods={"GET"})
     */
    public function show(Prize $prize): Response
    {
        return $this->render('prize/show.html.twig', [
            'prize' => $prize,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="prize_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Prize $prize): Response
    {
        $form = $this->createForm(PrizeType::class, $prize);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('prize_index');
        }

        return $this->render('prize/edit.html.twig', [
            'prize' => $prize,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="prize_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Prize $prize): Response
    {
        if ($this->isCsrfTokenValid('delete'.$prize->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($prize);
            $entityManager->flush();
        }

        return $this->redirectToRoute('prize_index');
    }
}
