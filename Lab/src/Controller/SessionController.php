<?php

namespace App\Controller;

use App\Entity\Session;
use App\Repository\TicketRepository;
use App\Repository\SessionRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/session")
 */
class SessionController extends AbstractController
{
    /**
     * @Route("/{page<\d+>?1}", name="session_index", methods={"GET", "POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY", message="Vous n'ètes pas connécté")
     */
    public function index(SessionRepository $sessionRepository, Request $request, $page, PaginatorInterface $paginator): Response
    {
        $limit = 10;

        $searchBar = $this->createFormBuilder()
            ->add('query', TextType::class,array(
                'label' => false,
                'attr' => [
                    'class' => 'input is-primary inputSearchTicket',
                    'placeholder' => '0092839203',
                ]
            ))
            ->add('search', SubmitType::class, [
                'label' => 'Rechercher',
                'attr' => [
                    'class' => 'button is-primary btnSearchAdmin',
                ]
            ])->getForm();

        $searchBar->handleRequest($request);

        $querySession = $request->request->get('form')['query'];
        $resSearch = $sessionRepository->searchSession($querySession);

        if ($resSearch) {
            $sessions = $paginator->paginate(
                $resSearch,
                $page,
                $limit
            );
        } else {
            $sessions = $paginator->paginate(
                $sessionRepository->findAllVisibleQuery(),
                $page,
                $limit
            );
        }    

        return $this->render('session/index.html.twig',[
            'sessions' => $sessions,
            'searchBar' => $searchBar->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="session_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Session $session): Response
    {
        if ($this->isCsrfTokenValid('delete'.$session->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($session);
            $entityManager->flush();
        }

        return $this->redirectToRoute('session_index');
    }

    /**
     * @Route("/show/{id}", name="session_show", methods={"GET"})
     */
    public function show(TicketRepository $ticketRepository, Session $session, $id): Response
    {
        return $this->render('session/show.html.twig', [
            'session' => $session,
            'ticketSession' => $ticketRepository->findBySession($id)
        ]);
    }
}
