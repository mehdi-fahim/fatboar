<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="user_sign")
     */
    public function signAction(AuthenticationUtils $authenticationUtils, Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // get and assign role 'ROLE_USER'
        $entityManager  = $this->getDoctrine()->getManager();
        $groupMember    = $entityManager->getRepository('App:Group')->findByRole('ROLE_USER');
        
        if(!empty($groupMember)){
            $user->addGroup($groupMember[0]);
        }
        $user->setImageUrl('https://freesvg.org/img/icon_user_greyonwhite.png');
        $user->setCheckCgu(false);
        $user->setCheckNewsletter(false);

        // dd($user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            // dd($user);

            // 4) save the User!
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/sign.html.twig',[
            'form' => $form->createView()
        ]);
    }

    // /**
    //  * @Route("/sign", name="user_registration")
    //  */
    // public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    // {
    //     // 1) build the form
    //     $user = new User();
    //     $form = $this->createForm(UserType::class, $user);

    //     // get and assign role 'ROLE_USER'
    //     $entityManager  = $this->getDoctrine()->getManager();
    //     $groupMember    = $entityManager->getRepository('App:Group')->findByRole('ROLE_USER');
    //     if(!empty($groupMember)){
    //         $user->addGroup($groupMember[0]);
    //     }

    //     // 2) handle the submit (will only happen on POST)
    //     $form->handleRequest($request);
    //     if ($form->isSubmitted() && $form->isValid()) {

    //         // 3) Encode the password (you could also do this via Doctrine listener)
    //         $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
    //         $user->setPassword($password);

    //         // 4) save the User!
    //         $entityManager->persist($user);
    //         $entityManager->flush();
    //         return $this->redirectToRoute('home');
    //     }

    //     return $this->render('security/login.html.twig',[
    //         'form' => $form->createView()
    //     ]);
    // }
}
