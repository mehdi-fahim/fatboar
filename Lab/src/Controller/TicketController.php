<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Form\TicketType;
use App\Repository\UserRepository;
use App\Repository\TicketRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * @Route("/admin/ticket")
 */
class TicketController extends AbstractController
{
    /**
     * @Route("/{page<\d+>?1}", name="ticket_index", methods={"GET", "POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY", message="Vous n'ètes pas connécté")
     */
    public function index(TicketRepository $ticketRepository, $page, Request $request, PaginatorInterface $paginator): Response
    {
        $limit = 10;

        $searchBar = $this->createFormBuilder()
            ->add('query', TextType::class,array(
                'label' => false,
                'attr' => [
                    'class' => 'input is-primary inputSearchTicket',
                    'placeholder' => '8729030392',
                ]
            ))
            ->add('search', SubmitType::class, [
                'label' => 'Rechercher',
                'attr' => [
                    'class' => 'button is-primary btnSearchAdmin',
                ]
            ])->getForm();

        $searchBar->handleRequest($request);

        $queryTicket = $request->request->get('form')['query'];
        $resSearch = $ticketRepository->searchTicket($queryTicket);

        if ($resSearch) {
            $ticket = $paginator->paginate(
                $resSearch,
                $page,
                $limit
            );
        } else {
            $ticket = $paginator->paginate(
                $ticketRepository->findAllVisibleQuery(),
                $page,
                $limit
            );
        }

        return $this->render('ticket/index.html.twig',[
            'tickets' => $ticket,
            'searchBar' => $searchBar->createView()
        ]);
    }

    /**
     * @Route("/new", name="ticket_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserRepository $userRepo): Response
    {
        $ticket = new Ticket();
        $form = $this->createForm(TicketType::class, $ticket);
        $form->handleRequest($request);

        $user = $this->getUser();

        if ($form->isSubmitted() && $form->isValid()) {

            $ticket->setUser($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ticket);
            $entityManager->flush();

            return $this->redirectToRoute('ticket_index');
        }

        return $this->render('ticket/new.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="ticket_show", methods={"GET","POST"})
     */
    public function show(Request $request, Ticket $ticket): Response
    {
        $form = $this->createForm(TicketType::class, $ticket);
        $form->handleRequest($request);

        $data = $form->getData();

        if ($form->isSubmitted() && $form->isValid()) {
            if(!$data->getIsRecover()) {
                $ticket->setIsRecover(false);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($ticket);
                $entityManager->flush();
                return $this->redirectToRoute('ticket_index');
            } else {
                $ticket->setIsRecover(true);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($ticket);
                $entityManager->flush();
                return $this->redirectToRoute('ticket_index');
            }
        }

        return $this->render('ticket/show.html.twig', [
            'ticket' => $ticket,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ticket_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Ticket $ticket): Response
    {
        // return $this->render('ticket/show.html.twig', [
        //     'ticket' => $ticket,
        //     'form' => $form->createView(),
        // ]);
    }

    /**
     * @Route("/{id}/delete", name="ticket_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Ticket $ticket): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ticket->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ticket);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ticket_index');
    }
}
