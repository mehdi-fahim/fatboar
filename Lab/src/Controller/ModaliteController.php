<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class ModaliteController extends AbstractController
{

    /**
     * @Route("/modalite", name="modalite")
     */
    public function index()
    {
        return $this->render('modalite/index.html.twig');  
    }
}


