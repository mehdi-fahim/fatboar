<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ContactController extends AbstractController
{

    /**
     * @Route("/contact", name="contact_new")
     * @return Response
     */
    public function new(Request $request, \Swift_Mailer $mailer): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $message = (new \Swift_Message('Hello Email'))
                ->setSubject($contact->getSubject())
                ->setFrom($contact->getEmail())
                ->setTo('belkacem.boughida@gmail.com')
                ->setBody(
                    $this->renderView( 'contact/email.html.twig', [ 
                        'message' => $contact->getMessage(),
                        'email' => $contact->getEmail()
                    ]), 'text/html');

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();
            $mailer->send($message);
            
            $this->addFlash('success', "Merci, votre demande vient d'être envoyé. Nous reviendrons vers vous dans les plus bref délais.");
            
            }
            return $this->render('contact/contact.html.twig', [
                'form' => $form->createView(),
            ]);  
        

        }
}