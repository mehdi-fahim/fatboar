<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use App\Entity\Prize;
use App\Entity\Ticket;
use App\Form\NewsLetterType;
use App\Repository\PrizeRepository;
use App\Repository\TicketRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/profil")
 */
class ProfilController extends AbstractController
{
    /**
     * @Route("/", name="profil_show", methods={"GET", "POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY", statusCode=401, message="Non autorisé")
     */
    public function getProfil(Security $security, Request $request, TicketRepository $ticketRepository, PrizeRepository $prizeRepository)
    {
        $user = $security->getUser();

        $objectToarrayUser = (array) $user;
        $idUser = $objectToarrayUser["\x00App\Entity\User\x00id"];

        $tickets = $ticketRepository->findBy(['user' => $idUser]);
        $totalTickets = count($tickets);

        $lots = $prizeRepository->findBy(['user' => $idUser]);
        $totalLots = count($lots);

        if($request->isMethod('POST') && !empty($request->get('ticket_no'))) {
            $ticketno = $request->get('ticket_no');
            $tickets = $ticketRepository->searchTicketByUser($ticketno, $idUser);
        } elseif($request->isMethod('POST') && !empty($request->get('name'))) {
            $lotsname = $request->get('name');
            $lots = $prizeRepository->findBy(['user' => $idUser, 'name' => $lotsname]);
        }

        $form = $this->createForm(NewsLetterType::class, $user);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }  

        return $this->render('profil/profil.html.twig', [
            'user' => $user,
            'tickets' => $tickets,
            'lots' => $lots,
            'totalTickets' => $totalTickets,
            'totalLots' => $totalLots,
            'form' => $form->createView(),
        ]);
    }
}
