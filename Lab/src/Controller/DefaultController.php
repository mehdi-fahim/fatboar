<?php
namespace App\Controller;

use App\Entity\User;
use App\Entity\Prize;
use App\Entity\Ticket;
use App\Entity\Session;
use App\Form\CheckModalType;
use App\Repository\TicketRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home", methods={"GET", "POST"})    
     */
    public function index(Request $request, TicketRepository $ticketRepository)
    {
        $inputHome = $this->createFormBuilder()
            ->add('query', TextType::class,array(
                'label' => false,
                'attr' => [
                    'class' => 'input is-primary formHome',
                    'placeholder' => '9283476528',
                ]
            ))
            ->add('search', SubmitType::class, [
                'label' => 'JOUER MAINTENANT',
                'attr' => [
                    'class' => 'button is-link',
                ]
            ])->getForm();

        $inputHome->handleRequest($request);
        $queryGetPrize = $request->request->get('form')['query'];

        $repository = $this->getDoctrine()->getRepository(Ticket::class);
        $checkTicket = $repository->findBy(['ticket_no' => $queryGetPrize]);

        $repository = $this->getDoctrine()->getRepository(Session::class);
        $session = $repository->find(1);

        $user = $this->getUser();

        $form = $this->createForm(CheckModalType::class, $user);
        $form->handleRequest($request);

        if($inputHome->isSubmitted()) {
            if ($checkTicket) {    
                if($checkTicket[0]->getIsUsed()) {
                    return $this->render('home/home.html.twig',[
                        'inputHome' => $inputHome->createView(),
                        'isUsed' => true,
                        'form' => $form->createView(),
                    ]);
                }
                
                $checkTicket[0]->setIsUsed(true);
                $checkTicket[0]->setUser($user);
                $checkTicket[0]->setSession($session);
                $checkTicket[0]->setdateUpdate(new \Datetime);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($checkTicket[0]);
                $entityManager->flush();
    
                return $this->render('home/home.html.twig',[
                    'inputHome' => $inputHome->createView(),
                    'checkTicket' => $checkTicket[0],
                    'form' => $form->createView(),
                ]);
            } elseif ($checkTicket == null)  {
                return $this->render('home/home.html.twig',[
                    'inputHome' => $inputHome->createView(),
                    'checkTicket' => null,
                    'form' => $form->createView(),
                ]);
            } else {
                return $this->render('home/home.html.twig',[
                    'inputHome' => $inputHome->createView(),
                    'form' => $form->createView(),
                ]);
            }
        } else {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
            }                


            return $this->render('home/home.html.twig',[
                'inputHome' => $inputHome->createView(),
                'form' => $form->createView(),
            ]);
        }
    }

    /**
     * @Route("/sitemap.xml", name="sitemap", methods={"GET"})    
     */
    public function sitemap()
    {
        return $this->render('sitemap/sitemap.xml');
    }
}
