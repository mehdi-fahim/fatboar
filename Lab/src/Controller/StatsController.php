<?php

namespace App\Controller;

use Mukadi\Chart\Chart;
use Mukadi\Chart\Builder;
use App\Repository\StatsRepository;
use Mukadi\Chart\Utils\RandomColorFactory;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StatsController extends AbstractController
{
    // /**
    //  * @Route("admin/stats", name="stats_index")
    //  * @IsGranted("IS_AUTHENTICATED_FULLY", message="Vous n'ètes pas connécté")
    //  */

    public function chart(StatsRepository $statsRepo) {

        $dsn = "pgsql:host=postgres;port=5432;dbname=fatboar;user=fatb;password=postgres";
        $conn = new \PDO($dsn);

        $builder = new Builder($conn);

        $nbTicketUsed[0]= $statsRepo->countTicketIsUsed();
        $nbTicketUsedPrize1[0]= $statsRepo->countTicketIsUsedPrize1();
        $nbTicketUsedPrize2[0]= $statsRepo->countTicketIsUsedPrize2();
        $nbTicketUsedPrize3[0]= $statsRepo->countTicketIsUsedPrize3();
        $nbTicketUsedPrize4[0]= $statsRepo->countTicketIsUsedPrize4();
        $nbTicketUsedPrize5[0]= $statsRepo->countTicketIsUsedPrize5();
        $nbTicketRecover[0]= $statsRepo->countTicketIsRecover();

        $ticketUsed = $nbTicketUsed[0][0][1];
        $ticketUsedPrize1 = $nbTicketUsedPrize1[0][0][1];
        $ticketUsedPrize2 = $nbTicketUsedPrize2[0][0][1];
        $ticketUsedPrize3 = $nbTicketUsedPrize3[0][0][1];
        $ticketUsedPrize4 = $nbTicketUsedPrize4[0][0][1];
        $ticketUsedPrize5 = $nbTicketUsedPrize5[0][0][1];
        $ticketRecover = $nbTicketRecover[0][0][1];

        $chart = $builder->query('SELECT COUNT(*) AVG(prix) prix, id FROM ticket');

        return $this->render('stats/index.html.twig',[
            "stats" => $chart,
            "ticketUsed" => $ticketUsed,
            "ticketRecover" => $ticketRecover,
            "ticketUsedPrize1" => $ticketUsedPrize1,
            "ticketUsedPrize2" => $ticketUsedPrize2,
            "ticketUsedPrize3" => $ticketUsedPrize3,
            "ticketUsedPrize4" => $ticketUsedPrize4,
            "ticketUsedPrize5" => $ticketUsedPrize5
        ]);
    }
}
