<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SessionRepository")
 */
class Session
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     */
    private $no_session;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="datetime")
     */
    private $date_valide_ticket;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="datetime")
     */
    private $date_debut_session;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_fin_session;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ticket", mappedBy="session")
     */
    private $ticket_id;

    public function __construct()
    {
        $this->ticket_id = new ArrayCollection();
        $this->date_debut_session = new \DateTime("Europe/Paris");
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNoSession(): ?string
    {
        return $this->no_session;
    }

    public function setNoSession(string $no_session): self
    {
        $this->no_session = $no_session;

        return $this;
    }

    public function getDateValideTicket(): ?\DateTimeInterface
    {
        return $this->date_valide_ticket;
    }

    public function setDateValideTicket(\DateTimeInterface $date_valide_ticket): self
    {
        $this->date_valide_ticket = $date_valide_ticket;

        return $this;
    }

    public function getDateDebutSession(): ?\DateTimeInterface
    {
        return $this->date_debut_session;
    }

    public function setDateDebutSession(\DateTimeInterface $date_debut_session): self
    {
        $this->date_debut_session = $date_debut_session;

        return $this;
    }

    public function getDateFinSession(): ?\DateTimeInterface
    {
        return $this->date_fin_session;
    }

    public function setDateFinSession(\DateTimeInterface $date_fin_session): self
    {
        $this->date_fin_session = $date_fin_session;

        return $this;
    }

    /**
     * @return Collection|Ticket[]
     */
    public function getTicketId(): Collection
    {
        return $this->ticket_id;
    }

    public function addTicketId(Ticket $ticketId): self
    {
        if (!$this->ticket_id->contains($ticketId)) {
            $this->ticket_id[] = $ticketId;
            $ticketId->setSession($this);
        }

        return $this;
    }

    public function removeTicketId(Ticket $ticketId): self
    {
        if ($this->ticket_id->contains($ticketId)) {
            $this->ticket_id->removeElement($ticketId);
            // set the owning side to null (unless already changed)
            if ($ticketId->getSession() === $this) {
                $ticketId->setSession(null);
            }
        }

        return $this;
    }
}
