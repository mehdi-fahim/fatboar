<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(schema="public")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=255)
     */
    private $first_name;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=255)
     */
    private $family_name;

    /**
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     *
     * @Assert\Length(
     *      min=5,
     *      max=50
     * )
     */
    private $plainPassword;

    /**
     * @Assert\NotBlank()
     * @Assert\Url(
     *    message = "The url '{{ value }}' is not a valid url",
     * )
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_url;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $token_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebook_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $google_id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="datetime", length=255)
     */
    private $date_create;

    /**
     * @ORM\Column(type="datetime", length=255, nullable=true)
     */
    private $date_delete;

    /**
     * @ORM\Column(type="boolean")
     */
    private $check_cgu;

    /**
     * @ORM\Column(type="boolean")
     */
    private $check_newsletter;

    /**
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users")
     */
    private $groups;

    /**
     * @ORM\OneToMany(targetEntity=Ticket::class, cascade={"persist", "remove"}, mappedBy="user")
     */
    private $ticket;

    /**
     * @ORM\OneToMany(targetEntity=User::class, cascade={"persist", "remove"}, mappedBy="user")
     */
    private $prize;

    public function __construct()
    {
        $this->date_create = new \DateTime("Europe/Paris");
        $this->groups = new ArrayCollection();
        $this->ticket = new ArrayCollection();
        $this->prize = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getFamilyName(): ?string
    {
        return $this->family_name;
    }

    public function setFamilyName(string $family_name): self
    {
        $this->family_name = $family_name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    public function setImageUrl(?string $image_url): self
    {
        $this->image_url = $image_url;

        return $this;
    }


    /**
     * Add group
     *
     * @param \App\Entity\Group $group
     *
     * @return User
     */
    public function addGroup(\App\Entity\Group $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param \App\Entity\Group $group
     */
    public function removeGroup(\App\Entity\Group $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Add ticket.
     *
     * @param \App\Entity\Ticket $ticket
     *
     * @return User
     */
    public function addTicket(\App\Entity\Ticket $ticket)
    {
        $this->ticket[] = $ticket;

        return $this;
    }

    /**
     * Remove ticket.
     *
     * @param \App\Entity\Ticket $ticket
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTicket(\App\Entity\Ticket $ticket)
    {
        return $this->ticket->removeElement($ticket);
    }

    /**
     * Get ticket.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Add prize.
     *
     * @param \App\Entity\Prize $prize
     *
     * @return User
     */
    public function addPrize(\App\Entity\Prize $prize)
    {
        $this->prize[] = $prize;

        return $this;
    }

    /**
     * Remove prize.
     *
     * @param \App\Entity\Prize $prize
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePrize(\App\Entity\Prize $prize)
    {
        return $this->prize->removeElement($prize);
    }

    /**
     * Get prize.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrize()
    {
        return $this->prize;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        $role = $this->groups->toArray(); 
        return array($role[0]->getRole());
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }


    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        return null;
    }

    /**
     * Set tokenId.
     *
     * @param int|null $tokenId
     *
     * @return User
     */
    public function setTokenId($tokenId = null)
    {
        $this->token_id = $tokenId;

        return $this;
    }

    /**
     * Get tokenId.
     *
     * @return int|null
     */
    public function getTokenId()
    {
        return $this->token_id;
    }

    /**
     * Set facebookId.
     *
     * @param string|null $facebookId
     *
     * @return User
     */
    public function setFacebookId($facebookId = null)
    {
        $this->facebook_id = $facebookId;

        return $this;
    }

    /**
     * Get facebookId.
     *
     * @return string|null
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set googleId.
     *
     * @param string|null $googleId
     *
     * @return User
     */
    public function setGoogleId($googleId = null)
    {
        $this->google_id = $googleId;

        return $this;
    }

    /**
     * Get googleId.
     *
     * @return string|null
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * Set dateCreate.
     *
     * @param \DateTime $dateCreate
     *
     * @return User
     */
    public function setDateCreate($dateCreate)
    {
        $this->date_create = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate.
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * Set dateDelete.
     *
     * @param \DateTime|null $dateDelete
     *
     * @return User
     */
    public function setDateDelete($dateDelete = null)
    {
        $this->date_delete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete.
     *
     * @return \DateTime|null
     */
    public function getDateDelete()
    {
        return $this->date_delete;
    }

    public function getCheckCgu(): ?bool
    {
        return $this->check_cgu;
    }

    public function setCheckCgu(bool $check_cgu): self
    {
        $this->check_cgu = $check_cgu;

        return $this;
    }

    public function getCheckNewsletter(): ?bool
    {
        return $this->check_newsletter;
    }

    public function setCheckNewsletter(bool $check_newsletter): self
    {
        $this->check_newsletter = $check_newsletter;

        return $this;
    }
}
