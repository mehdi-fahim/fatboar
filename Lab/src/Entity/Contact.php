<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email(message="Veuillez entrer une adresse e-mail valide")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=25, minMessage="Veuillez entrer au minimum 3 caractères", maxMessage="Veuillez entrer au maximum 25 caractères")
     * @Assert\Regex( pattern="/^[A-Za-z0-9 _]+[\ \-]*[A-Za-z0-9][A-Za-z0-9 _]*$/", message="Ce champ doit comporter les caractères suivant uniquement A-Z a-z 0-9 - " )
     */
    private $subject;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Assert\Length(min=10, minMessage="Veuillez entrer au minimum {{ limit }} caractères")
     */
    private $message;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_create;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_delete;

    public function __construct()
    {
        $this->date_create = new \DateTime("Europe/Paris");
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Set dateCreate.
     *
     * @param \DateTime $dateCreate
     *
     * @return Contact
     */
    public function setDateCreate($dateCreate)
    {
        $this->date_create = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate.
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * Set dateDelete.
     *
     * @param \DateTime|null $dateDelete
     *
     * @return Contact
     */
    public function setDateDelete($dateDelete = null)
    {
        $this->date_delete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete.
     *
     * @return \DateTime|null
     */
    public function getDateDelete()
    {
        return $this->date_delete;
    }
}
