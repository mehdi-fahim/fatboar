<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TicketRepository")
 */
class Ticket
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @ORM\Column(type="text")
     */
    private $ticket_no;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @ORM\Column(type="integer")
     */
    private $prix;

    /**
     * @Assert\Type("bool")
     * @ORM\Column(type="boolean")
     */
    private $is_used;

    /**
     * @Assert\Type("bool")
     * @ORM\Column(type="boolean")
     */
    private $is_recover;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_create;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_update;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_delete;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="ticket")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Prize::class, inversedBy="ticket")
     */
    private $prize;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Session", inversedBy="ticket_id")
     */
    private $session;

    public function __construct()
    {
        $this->date_create = new \DateTime("Europe/Paris");
        $this->prize = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTicketNo(): ?int
    {
        return $this->ticket_no;
    }

    public function setTicketNo(int $ticket_no): self
    {
        $this->ticket_no = $ticket_no;

        return $this;
    }

    public function getDateCreate(): ?\DateTimeInterface
    {
        return $this->date_create;
    }

    public function setDateCreate(\DateTimeInterface $date_create): self
    {
        $this->date_create = $date_create;

        return $this;
    }

    public function getDateUpdate(): ?\DateTimeInterface
    {
        return $this->date_update;
    }

    public function setDateUpdate(\DateTimeInterface $date_update): self
    {
        $this->date_update = $date_update;

        return $this;
    }

    public function getDateDelete(): ?\DateTimeInterface
    {
        return $this->date_delete;
    }

    public function setDateDelete(\DateTimeInterface $date_delete): self
    {
        $this->date_delete = $date_delete;

        return $this;
    }

    /**
     * Set user.
     *
     * @param \App\Entity\User|null $user
     *
     * @return Ticket
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \App\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add prize.
     *
     * @param \App\Entity\Prize $prize
     *
     * @return Ticket
     */
    public function addPrize(\App\Entity\Prize $prize)
    {
        $this->prize = $prize;

        return $this;
    }

    /**
     * Remove prize.
     *
     * @param \App\Entity\Prize $prize
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePrize(\App\Entity\Prize $prize)
    {
        return $this->prize->removeElement($prize);
    }

    /**
     * Get prize.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrize()
    {
        return $this->prize;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(?int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getIsUsed(): ?bool
    {
        return $this->is_used;
    }

    public function setIsUsed(bool $is_used): self
    {
        $this->is_used = $is_used;

        return $this;
    }

    public function getIsRecover(): ?bool
    {
        return $this->is_recover;
    }

    public function setIsRecover(bool $is_recover): self
    {
        $this->is_recover = $is_recover;

        return $this;
    }
}
