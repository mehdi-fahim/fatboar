<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\User;
use App\Entity\Group;
use App\Entity\Prize;
use App\Entity\Ticket;
use App\Entity\Contact;
use App\Entity\Session;
use App\Entity\TypePrize;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

class AppFixtures extends Fixture
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function cacul_pourcentage($total,$pourcentage)
    { 
        $resultat = $total * $pourcentage;
        return round($resultat); // Arrondi la valeur
    }   

    public function load(ObjectManager $manager)
    {
 
        // On configure dans quelles langues nous voulons nos données
        $faker = Faker\Factory::create('fr_FR');

        $session = new Session();
        $session->setNoSession($faker->numberBetween(111111111, 999999999));
        $session->setDateValideTicket($faker->dateTimeThisMonth($max = 'now', $timezone = 'Europe/Paris'));
        $datDebutSession = $faker->dateTimeThisMonth($max = 'now', $timezone = 'Europe/Paris');
        $session->setDateDebutSession($datDebutSession);
        $session->setDateFinSession($faker->dateTimeBetween($startDate = $datDebutSession, $endDate = '+ 30 days', $timezone = 'Europe/Paris'));
        $manager->persist($session);

        // Création des rôles 
        $role1 = new Group();
        $role1->setName("Utilisateur");
        $role1->setRole("ROLE_USER");

        $role2 = new Group();
        $role2->setName("Employée");
        $role2->setRole("ROLE_EMP");
        
        $role3 = new Group();
        $role3->setName("Administrateur");
        $role3->setRole("ROLE_ADMIN");

        $manager->persist($role1);
        $manager->persist($role2);
        $manager->persist($role3);
        
        // Flush dans la BDD
        $manager->flush();

        //Creation d'un admin
        $user = new User();
        $user->setFirstName("Fatboar");
        $user->setFamilyName("Admin");
        $user->setEmail("fatboar.adm@gmail.com");
        $user->setImageUrl("http://www.astrosurf.com/uploads/monthly_2017_12/F.png.d945a30e9123093b93f5adc31bec356d.png");
        $user->setDateCreate($faker->dateTimeThisMonth($max = 'now', $timezone = 'Europe/Paris'));
        $user->setPassword(null);
        $user->setCheckCgu(true);
        $user->setCheckNewsletter(false);
        $groupMember = $this->em->getRepository('App:Group')->findByRole('ROLE_ADMIN');
        if(!empty($groupMember)){
            $user->addGroup($groupMember[0]);
        }
        $manager->persist($user); 

        //Creation d'un employé
        $user = new User();
        $user->setFirstName("Fatboar");
        $user->setFamilyName("Employe");
        $user->setEmail("fatboar.emp@gmail.com");
        $user->setImageUrl("https://lh3.googleusercontent.com/-uRc-don34eQ/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rf07SSaO75w-cXXId7rCF55MeCNAw/photo.jpg");
        $user->setDateCreate($faker->dateTimeThisMonth($max = 'now', $timezone = 'Europe/Paris'));
        $user->setPassword(null);
        $user->setCheckCgu(true);
        $user->setCheckNewsletter(false);
        $groupMember = $this->em->getRepository('App:Group')->findByRole('ROLE_EMP');
        if(!empty($groupMember)){
            $user->addGroup($groupMember[0]);
        }
        $manager->persist($user); 

        // Flush dans la BDD
        $manager->flush();

        //Création des différent prix 

        $prize = new Prize();
        $prize->setName("Entrée ou Dessert au choix");
        $prize->setContent("https://static.vecteezy.com/system/resources/thumbnails/000/240/531/original/milkshake-cafe-or-restaurant-logo-or-illustration.jpg");
        $prize->setDateCreate($faker->dateTimeThisMonth($max = 'now', $timezone = 'Europe/Paris'));
        $prize->setDateUpdate($faker->dateTimeInInterval($startDate = 'now', $interval = '+1 year', $timezone = null));
        $prize->setDateDelete($faker->dateTimeInInterval($startDate = '+1 year', $interval = '+2 years', $timezone = null));
        $manager->persist($prize);

        $prize = new Prize();
        $prize->setName("Burgeur");
        $prize->setContent("https://tmbidigitalassetsazure.blob.core.windows.net/secure/RMS/attachments/37/1200x1200/exps28800_UG143377D12_18_1b_RMS.jpg");
        $prize->setDateCreate($faker->dateTimeThisMonth($max = 'now', $timezone = 'Europe/Paris'));
        $prize->setDateUpdate($faker->dateTimeInInterval($startDate = 'now', $interval = '+1 year', $timezone = null));
        $prize->setDateDelete($faker->dateTimeInInterval($startDate = '+1 year', $interval = '+2 years', $timezone = null));
        $manager->persist($prize);

        $prize = new Prize();
        $prize->setName("Menu du jour");
        $prize->setContent("https://media-cdn.tripadvisor.com/media/photo-s/11/47/16/71/menu-burger-fils-a-papa.jpg");
        $prize->setDateCreate($faker->dateTimeThisMonth($max = 'now', $timezone = 'Europe/Paris'));
        $prize->setDateUpdate($faker->dateTimeInInterval($startDate = 'now', $interval = '+1 year', $timezone = null));
        $prize->setDateDelete($faker->dateTimeInInterval($startDate = '+1 year', $interval = '+2 years', $timezone = null));
        $manager->persist($prize);

        $prize = new Prize();
        $prize->setName("Menu aux choix");
        $prize->setContent("https://realdeals.ch/media/catalog/product/cache/4/image/9df78eab33525d08d6e5fb8d27136e95/s/p/spring_brothers_burger_frites_maison_salade_1_2.jpg");
        $prize->setDateCreate($faker->dateTimeThisMonth($max = 'now', $timezone = 'Europe/Paris'));
        $prize->setDateUpdate($faker->dateTimeInInterval($startDate = 'now', $interval = '+1 year', $timezone = null));
        $prize->setDateDelete($faker->dateTimeInInterval($startDate = '+1 year', $interval = '+2 years', $timezone = null));
        $manager->persist($prize);

        $prize = new Prize();
        $prize->setName("-70% de réduction");
        $prize->setContent("https://img.freepik.com/free-photo/70-percent-discount-3d-text_2227-137.jpg?size=626&ext=jpg");
        $prize->setDateCreate($faker->dateTimeThisMonth($max = 'now', $timezone = 'Europe/Paris'));
        $prize->setDateUpdate($faker->dateTimeInInterval($startDate = 'now', $interval = '+1 year', $timezone = null));
        $prize->setDateDelete($faker->dateTimeInInterval($startDate = '+1 year', $interval = '+2 years', $timezone = null));
        $manager->persist($prize);

        // Flush dans la BDD
        $manager->flush();

        //Répartition des prix par ticket
        $prize1 = $this->em->getRepository(Prize::class)->findByName('Entrée ou Dessert au choix');
        $prize2 = $this->em->getRepository('App:Prize')->findByName('Burgeur');
        $prize3 = $this->em->getRepository('App:Prize')->findByName('Menu du jour');
        $prize4 = $this->em->getRepository('App:Prize')->findByName('Menu aux choix');
        $prize5 = $this->em->getRepository('App:Prize')->findByName('-70% de réduction');

        // Requète pour récupéré le total de ticket
        $qb = $this->em->createQueryBuilder();
        $qb->select('count(ticket.id)');
        $qb->from('App:Ticket','ticket');
        $count = $qb->getQuery()->getSingleScalarResult();

        //On assigne le prix n°1 à 60% des tickets
        for ($i = 0; $i <= $this->cacul_pourcentage($count, 0.6); $i++) {
            $ticketRes = $this->em->getRepository(Ticket::class)->find($i);
            if(!empty($ticketRes)){
                if(empty($ticketRes->getPrize())) {
                    $ticketRes->addPrize($prize1[0]);
                    $manager->persist($ticketRes);
                } else {
                    $i++;
                }
            }
        }
        $manager->flush();

        for ($i = $this->cacul_pourcentage($count, 0.6) + 1; $i <= $this->cacul_pourcentage($count, 0.8); $i++) {
            $ticketRes = $this->em->getRepository(Ticket::class)->find($i);
            if(!empty($ticketRes)){
                if(empty($ticketRes->getPrize())) {
                    $ticketRes->addPrize($prize2[0]);
                    $manager->persist($ticketRes);
                } else {
                    $i++;
                }
            }
        }
        $manager->flush();

        for ($i = $this->cacul_pourcentage($count, 0.8) + 1; $i <= $this->cacul_pourcentage($count, 0.9); $i++) {
            $ticketRes = $this->em->getRepository(Ticket::class)->find($i);
            if(!empty($ticketRes)){
                if(empty($ticketRes->getPrize())) {
                    $ticketRes->addPrize($prize3[0]);
                    $manager->persist($ticketRes);
                } else {
                    $i++;
                }
            }
        }
        $manager->flush();

        for ($i = $this->cacul_pourcentage($count, 0.9) + 1; $i <= $this->cacul_pourcentage($count, 0.96); $i++) {
            $ticketRes = $this->em->getRepository(Ticket::class)->find($i);
            if(!empty($ticketRes)){
                if(empty($ticketRes->getPrize())) {
                    $ticketRes->addPrize($prize4[0]);
                    $manager->persist($ticketRes);
                } else {
                    $i++;
                }
            }
        }
        $manager->flush();

        for ($i = $this->cacul_pourcentage($count, 0.96) + 1; $i <= $this->cacul_pourcentage($count, 1); $i++) {
            $ticketRes = $this->em->getRepository(Ticket::class)->find($i);
            if(!empty($ticketRes)){
                if(empty($ticketRes->getPrize())) {
                    $ticketRes->addPrize($prize5[0]);
                    $manager->persist($ticketRes);
                } else {
                    $i++;
                }
            }
        }
        $manager->flush();
    }
}
