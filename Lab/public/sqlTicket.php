<?php
ini_set('memory_limit', '-1');

/**
 * @ARGUMENT $nt(description="nombre de ticket à injecter dans la db", type=int)
 */
function creationTickets($nt)
{
    $timeZone = 'Europe/Paris';  // +2 hours
    $dateTime = new DateTime('now', new DateTimeZone('GMT'));
    $conn = pg_connect("host=db dbname=fatboar user=fatboar password=fatboar_password");
    $all_tickets = pg_query($conn, "SELECT * FROM ticket");
    $tickets_rows = pg_fetch_all($all_tickets);
    $last_ticket = end($tickets_rows);
    for ($i = 1; $i < $nt; $i++) {
        $progress = $i/$nt;
        system("clear");
        echo number_format( $progress * 100, 2 )." % \n";
        $ticket_num = $last_ticket['id'] + $i;
        pg_query($conn, "INSERT INTO ticket VALUES (" . $ticket_num . ", null, null, null," . rand(11111111111, 99999999999) . ", " . rand(18, 100) . ", false, false, '" . $dateTime->format("Y-m-d H:i:s") . "', null, null)");
    }
}

if ($argv[1] != 0 && $argv[1] > 0) {
    creationTickets($argv[1]);
} else {
    echo "Veuillez renseigner le nombre de ticket à insérer";
}
