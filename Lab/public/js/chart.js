var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Entrée ou Dessert au choix', 'Burgeur', 'Menu du jour', 'Menu aux choix', '-70% de réduction'],
        datasets: [{
            label: '# of Votes',
            data: [60, 20, 10, 6, 4],
            backgroundColor: [
                'rgba(255, 220, 0)',
                'rgba(255, 182, 0)',
                'rgba(255, 120, 0)',
                'rgba(200, 157, 0)',
                'rgba(200, 94, 0)',
            ],
            borderColor: [
                'rgba(255, 255, 255)',
                'rgba(255, 255, 255)',
                'rgba(255, 255, 255)',
                'rgba(255, 255, 255)',
                'rgba(255, 255, 255)',

            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});