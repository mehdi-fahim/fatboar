$(document).ready(function () {
    $('.js-user-autocomplete').each(function () {
        var autocompleteUrl = $(this).data('autocomplete-url');
        $(this).autocomplete({ hint: false }, [
            {
                source: function (query, cb) {
                    $.ajax({
                        url: autocompleteUrl
                    }).then(function (data) {
                        cb(data.users);
                    });
                },
                displayKey: 'ticketno',
                debounce: 500 // only request every 1/2 second
            }
        ])
    });
});