var btn = document.querySelector('#showModal');
var modalDlg = document.querySelector('#image-modal');
var modalBis = document.querySelector('#modal-bis');
var imageModalCloseBtn = document.querySelector('#image-modal-close');
var btnCancel = document.querySelector('#btn-cancel');
var btnClose = document.querySelector('#modal-close');

if(btn) {
  btn.addEventListener('click', function(){
    modalDlg.classList.add('is-active');
  });
}

if(imageModalCloseBtn) {
  imageModalCloseBtn.addEventListener('click', function(){
    modalDlg.classList.remove('is-active');
  });
}
if(btnCancel) {
  btnCancel.addEventListener('click', function(){
    modalDlg.classList.remove('is-active');
  });
}
if(btnClose) {
  btnClose.addEventListener('click', function(){
    modalBis.classList.remove('is-active');
  });
}