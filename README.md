# Fatboar

## Prérequis

* Docker dernière version
* ddocker-compose dernière version
* python 2.7
* Ansible==2.9.1

## Installation 

### Local

1. set .env et .env.db
  * copier le .env.dist
  * remplacer dans DATABASE_URL le "DB_USER", "DB_NAME", "DB_USER_PASSWORD", 
2. Le .env.db mettre les variable correspondant a la database
3. ./dockerkit install local

* http://localhost:8000 = Fatboar site
* http://localhost:5050 = Adminer


## Identifiant

* Fatboar
  * compte admin (connexion via google): 
    * fatboar.emp@gmail.com
    * Fatboar2019

* AWS
  * examinateur_dsp
  * examinateur_dsp2019!

* Autres Outils (gitlab, Jenkins, grafana)
  * examinateur_dsp
  * examinateur_dsp2019!
